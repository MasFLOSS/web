---
layout: post
lang: es
title: "Buscador mwmbl"
date: 2023-08-03 20:38:42 +0100
categories: recommendations
---

Esta es mi primera publicación desde hace algunos meses --aproximadamente 10-- debido a «cuestiones laborales».

Tengo algún tutorial por publicar pero es necesario que le realice alguna limpieza previa ya que está adaptado para un uso concreto y hay que redactarlo de forma genérica.

Entre esas «cuestiones laborales» he estado participando en un proyecto conocido como [mwmbl](https://mwmbl.org/).

## Mini introducción

De forma simple, se trata de un buscador sin ánimo de lucro creado por un equipo de 2 personas.

Está desarrollado en el lenguaje de programación Python y externaliza la parte de indexación de sitios web.

Básicamente, el servidor principal recoge listados de sitios web de una lista previamente analizada, y las herramientas de indexación se conectan al mismo, descargan un conjunto de webs a indexar, realizan la operación y devuelven el resultado al servidor.

El buscador es actualmente usable y es accesible desde su sitio web inicial con un índice ya considerablemente grande y rondando más de medio millón de sitios indexados al día.

Es mucho más modesto que los buscadores mayoritarios y carece de funciones avanzadas, pero sirve para búsquedas de texto completas.

## Participación

Existen varias formas de participar en el proyecto:  
  
* Donando: yo recomiendo hacerlo a través de [su colectivo en OpenCollective](https://opencollective.com/mwmbl/) cuyos fondos van destinados a hacer crecer la infraestructura inicial.
* Indexando:  
	- Se puede ayudar usando la extensión para [Mozilla Firefox](https://addons.mozilla.org/en-GB/firefox/addon/mwmbl-web-crawler/).
	- O ejecutando el [servicio de indexación](https://github.com/mwmbl/crawler-script/) (**aviso**: se hospeda en GitHub) en un servidor propio.
* Programando: si sabes algo de Python o incluso JavaScript para el frontal web o su extensión es posible que puedas participar en sus [repositorios](https://github.com/mwmbl/) (**aviso**: se hospeda en GitHub).

---

**Redacción:** [EchedelleLR](/redaction.html#EchedelleLR)
