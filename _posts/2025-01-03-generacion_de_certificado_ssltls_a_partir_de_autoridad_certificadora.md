---
layout: post
lang: es
title: "OpenSSL: generación de certificado SSL/TLS a partir de autoridad certificadora"
date: 2025-01-03 13:15:27 +0000
categories: tutorials
---

Esta entrada la he escrito para no redactar un procedimiento que, si bien puedo automatizar, realizo a veces en algún servidor de forma concreta y, al investigar un poco por no tener algo archivado para mi misma, me he fijado que hay mucha guías de mala calidad.

La idea es tener algo centralizado que pueda usar de guía. Está enfocada en OpenSSL específicamente.

## Resumen

Los pasos a realizar suelen ser siempre los mismos:
1. Extraer clave privada y certificado público del fichero de la autoridad certificadora
2. Generar clave privada personalizada
3. Generar petición de firma
4. Firmar la petición con la clave privada de la autoridad certificadora para generar el certificado público

## Guía

1. Generalmente, muchos proveedores suelen dar un formato PFX para la autoridad certificadora. El formato real es PKCS12 y de la siguiente forma extraeríamos la clave privada.
	```
	openssl pkcs12 -in CA.pfx -nocerts -out CA.key -nodes 
	```
	Opciones específicas usadas:
	* `pkcs12`: indica que se van a realizar operaciones objetos PKCS12
	* `-in`: indica el fichero de entrada
	* `-nocerts`: especifica que no se extraigan certificados
	* `-out`: nombre del fichero de salida
	* `-nodes`: especifica que no se cifre la clave privada

2. Extraemos también el certificado público.
	```
	openssl pkcs12 -in CA.pfx -nokeys -out CA.crt
	```
	Opciones específicas usadas:
	* `-nokeys`: especifica que no se extraigan claves privadas

3. Seguidamente, nos generamos una clave privada para su uso.
	```
	openssl genpkey -algorithm ed25519 -out server.key
	```
	Opciones específicas usadas:
	* `genpkey`: indica que vamos a generar una clave privada
	* `-algorithm`: especifica el algoritmo de claves a usar, en este caso `ed25519`

4. Generamos la petición de firma.
	```
	openssl req -new -sha512 -key server.key -out server.csr
	```
	Opciones específicas usadas:
	* `req`: indica que se van a realizar operaciones con una petición de firma
	* `-new`: indica que se va a generar una nueva
	* `-sha512`: especifica el algoritmo de integridad de la misma, en este caso SHA512

5. Finalmente, usamos las clave y certificado del CA para firmar petición de firma y generar el certificado público.
	```
	openssl x509 -req -in server.csr -CA CA.crt -CAkey CA.key -CAcreateserial -out server.crt -days 365 -sha512
	```
	Opciones específicas usadas:
	* `x509`: indica que se van a realizar operaciones con certificados X.509
	* `-req`: indica que se van a firmar una petición de firma
	* `-CA`: indica el certificado público de la autoridad certificadora
	* `-CAkey`: indica la clave privada de la autoridad certificadora
	* `-CAcreateserial`: genera un código serial y lo asigna al certificado permitiendo identificarlos
	* `-days`: especifica el número de días de validez

Y con esto estaría listo para ser usado en servidores RADIUS, web, etc.

---

**Redacción:** [EchedelleLR](/redaction.html#EchedelleLR)