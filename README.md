# Web
Este proyecto consiste en un sitio web estático desarrollado con [Jekyll](https://jekyllrb.com/) con el tema [no-style-please](https://riggraz.dev/no-style-please/) a modo de blog para redactar sobre el Software Libre y de Código Abierto.

## Estructura
El sitio web consta de una página inicial declarada solo con la cabecera como fichero `index.md` en [Markdown](https://daringfireball.net/projects/markdown/), specificada en formato [YAML](https://yaml.org/) y situada en `_data/menu.yml` a modo de lista. En la misma, se declaran los enlaces a las distintas publicaciones y resto de páginas del sitio.

Adicionalmente, existe una página de redacción declarada en el fichero `redaction.md` donde se especifican las personas que redactan los artículos.

Las publicaciones, están escritas en Markdown, adjuntando una cabecera adicional descrita más adelante, y situadas en `_posts/`.

Por defecto, en la página inicial no se listan todas las publicaciones, sino que tienen un máximo definido y pasando al archivo cuando sobre pasa el límite. El archivo es un conjunto de ficheros en Markdown que constan de una única cabecera y están situados en `archive/`. Existe uno general y otro por cada categoría.

Los recursos, utilizados tanto por páginas como publicaciones, se encuentran en el directorio `resources/` en sus respectivos subdirectorios `images/`, `videos/` y `other/` respectivamente.

Las configuraciones generales del sitio se declaran en el fichero `_config.yml`, y el fichero Gemfile indica las dependencias de [Ruby](https://www.ruby-lang.org/) que necesita el sitio.

La parte de la estructura no cubierta aquí se encuentra en la sección de [Problemas](#problemas).

## Funcionamiento
La web es actualizada cada 15 minutos usando un script en la [crontab](https://pubs.opengroup.org/onlinepubs/007908775/xcu/crontab.html) del sistema ejecutado sobre la cuenta donde está instalado Jekyll.

El script actualiza la copia del repositorio local, descarga o instala las dependencias de Ruby correspondientes y genera compila el sitio desde los ficheros fuente.

El script es el fichero `update.sh`.

## Participación
Cualquier persona puede participar en la redacción de artículos de este sitio web siguiendo las directrices especificadas posteriormente.

### Objetivos
Si bien la descripción del sitio web ya da indicios de lo que se comenta, es conveniente aclarar cuales son los objetivos a cumplir al elaborar artículos.

Así como se mencionan, son objetivos, no reglas estrictas, dado que pueden existir casos en los que sea imposible evitar alguna, pero se aspirará a ello en la medida de lo posible.

Estos son:
* Redactar exclusivamente de Software Libre y de Código Abierto
* Utilizar una expresión natural y neutral
* Ser neutral en asuntos ajenos a lo que se redacta
* Ser respetuoso con otros individuos

### Páginas
Las páginas son los elementos estáticos con menos flexibilidad del sitio web.

#### Tipos
Actualmente, el sitio web cuenta con 3 tipos de páginas, declaradas en base a las plantillas del tema:
* Básico
* Complejo
  * Inicio
  * Archivo

La diferencia principal entre cada tipo radica en la declaración de sus cabeceras donde las básicas cuentan con un número limitado y permanente de atributos y las complejas lo varían.

El tipo «Inicio» está reservado para la página de inicio del sitio web mientras del resto se pueden crear tantos como se necesiten.

#### Redacción
La creación y redacción de una página es determinado por acuerdo previo con el resto del personas que participan en la redacción.

Para la redacción, es necesario tener en cuenta el formato y situación del fichero que representa cada página, lo que hay que incluir en su cabecera y lo acordado para el contenido.

##### Fichero
Las nuevas páginas son ficheros con extensión `.md` creados en la raíz de las fuentes del sitio web. Sus nombres deberán ser cortos, de una o dos palabras y en minúscula.

En el caso de haber una o más páginas que contengan algo en común como las páginas del archivo, deberán ser agrupadas en un subdirectorio nombrandolo de forma que se identifique aquello que tienen las páginas en común y siguiendo la misma regla que para los nombres de las páginas.

##### Cacebera
Para explicar la estructura de la cabecera es necesario un ejemplo de una compleja utilizada en una página de archivo de categoría.

```
---
layout: archive
lang: es
which_category: news
title: Publicaciones de noticias
---
```

Cada uno de los elementos previos a los dos puntos, entre el bloque de tres guiones (`---`), es un atributo de la cabecera, junto a su correspondiente valor a la derecha de los dos puntos.

###### layout
Es el primer atributo básico y obligatorio de toda cabecera. Declara el tipo de página.

Las complejas tienen nombres propios como la actual y las simples se declaran como `page`.

Los valores posibles son los siguientes:
* `page`
* `home`
* `archive`

###### lang
Es el segundo atributo básico y el primero de los opcionales. Declara el idioma establecido para el contenido de la página.

Su valor es el mismo que lleva el atributo `lang` asociado a la etiqueta `HTML` al crear páginas en [HTML](https://www.w3.org/html/).

Posee el valor por defecto `en` por lo que, aunque no es obligatorio, es necesario establecer el atributo en la creación de cada página para indicar que está en español.

###### which_category
Es el primer y único atributo complejo y el segundo de los opcionales, específico para las páginas de archivos. Declara la categoría de las publicaciones a listar en la misma.

Su valor se corresponde con los mismos nombres de las categorías en inglés y en minúscula, quedando de la siguiente forma:
* `news`
* `recommendations`
* `tutorials`
* `opinions`
* `events`
* `announcements`

###### title
Es el tercer atributo básico y el segundo de los obligatorios. Declara el título de la página.

Es necesario que el título tenga una relación directa con la página y contenga una o dos palabras indicando su contenido.

##### Contenido
El contenido de las páginas suele mantenerse sin modificar durante bastante tiempo a excepción de las que listan publicaciones, que lo realiza de forma automática, y la que representa el equipo de redacción.

Como se ha indicado anteriormente, debe estar en Markdown.

### Publicaciones
Las publicaciones son los elementos estáticos con mayor flexibilidad del sitio web.

Son, elementalmente, un tipo de páginas complejas con una serie de atributos específicos.

#### Categorías
Las categorías clasifican las publicaciones por tema.

Existen 4 categorías iniciales: 
* Noticias
* Recomendaciones
* Tutoriales
* Opiniones
* Eventos
* Anuncios

Para añadir nuevas categorías hay que llegar a un acuerdo entre todas y cada una de las personas que participan en la redacción. Una vez acordada, la categoría simplemente se empezará a utilizar en los nuevos artículos.

Adicionalmente, tras la creación de una categoría, es necesario añadir el listado de artículos a la página principal y crear su fichero de archivo.

#### Redacción
La creación y redacción de una publicación es determinado por cualquier persona que quiera participar en la redacción.

Para la redacción, es necesario tener en cuenta el formato del fichero que representa cada publicación, lo que hay que incluir en su cabecera y cumplir los objetivos al elaborar el contenido. 

Por otro lado, la persona o personas encargadas de redactar el artículo deberán añadirse en el fichero de redacción usando títulos de segundo nivel para pseudónimos usados para representarse y una descripción propia como introducción.

##### Fichero
Las publicaciones se crean como ficheros sueltos con extensión `.md` en el directorio `_posts`. Sus nombres deberán incluir la fecha en el formato `AAAA-mm-dd` seguido con un guión (`-`) y, finalmente, el título de la publicación en minúscula usando caracteres ASCII.

##### Cabecera
Para explicar la estructura de la cabecera es necesario un ejemplo de una compleja utilizada en una publicación de prueba.

```
---
layout: post
lang: es
title: "Welcome to Jekyll!"
date: 2021-01-03 13:31:05 -0800
categories: news
---
```

Cada uno de los elementos previos a los dos puntos, entre el bloque de tres guiones (`---`), es un atributo de la cabecera, junto a su correspondiente valor a la derecha de los dos puntos.

###### layout
Es el primer atributo básico y obligatorio de toda cabecera. Declara el tipo de página.

Su valor, para indicar que es una publicación, debe ser `post`.

###### lang
Es el segundo atributo básico y el primero de los opcionales. Declara el idioma establecido para el contenido de la publicación.

Su valor es el mismo que lleva el atributo `lang` asociado a la etiqueta `HTML` al crear páginas en [HTML](https://www.w3.org/html/).

Posee el valor por defecto `en` por lo que, aunque no es obligatorio, es necesario establecer el atributo en la creación de cada publicación para indicar que está en español.

###### title
Es el tercer atributo básico y el segundo de los obligatorios. Declara el título de la publicación.

Es necesario que el título tenga una relación directa con la publicación, contenga varias palabras, indique algo de su contenido y además intente ser llamativo.

###### date
Es el primer atributo complejo y el tercero de los obligatorios. Declara la fecha de la publicación.

La fecha debe seguir el formato `AAAA-mm-dd HH:MM:SS DIFERENCIA-HORARIA` en cada publicación.

Una buena forma de obtenerla es especificándola como entrada al programa `date` de la siguiente forma:
```
date +"%Y-%m-%d %H:%M:%S %z"
```

###### categories
Es el segundo atributo complejo y el segundo de los opcionales. Declara la categoría de la publicación.

Su valor se corresponde con los mismos nombres de las categorías en inglés y en minúscula, quedando de la siguiente forma:
* `news`
* `recommendations`
* `tutorials`
* `opinions`
* `events`
* `announcements`

Como sugiere el nombre, este atributo soporta especificar más de una categoría pero en este sitio web solo utilizaremos una por artículo.

##### Contenido
El contenido de las publicaciones deberá seguir los objetivos marcados en el sitio web.

Al final de cada publicación, deberá incluirse un pie con las personas que han participado en la redacción y un enlace de cada una hasta su sección en dicha página.

La estructura del pié deberá ser de la siguiente forma:
```

---

**Redacción:** [Persona1](/redaction.html#persona1), [Persona2](/redaction.html#persona1)
```

Los 3 guiones (`---`), que anteceden el indicativo de quien redactó la publicación, conforman una línea horizontal de separación pero, tal y como se muestra, para que esta línea se cree correctamente, debe estar precedida y sucedida por una línea en blanco.

## Recursos
El sitio web puede contar con recursos utilizados a lo largo de publicaciones y/o páginas. Su fin es proveer alguna utilidad adicional a modo de explicación o ejemplificación, así como realzar el contenido.

Cabe destacar que todo recurso utilizado debe ser bajo respectivo de las personas que lo crearon. Es por ello que el contenido debería ser preferiblemente de fuente propia y, en caso contrario, disponible bajo una licencia libre.

Para los casos de recurrir a fuentes externas, debajo de donde se enlaza el recurso si está integrado o en el lugar de destino si está enlazado, deberá incluirse indicaciones de las personas que lo crearon y su licencia.

Existen tres tipos de recursos:
* Imágenes
* Vídeos
* Otros

### Imágenes
Este tipo de recurso es almacenado dentro del propio servidor.

#### Fichero
Cada fichero se almacena en un subdirectorio del directorio `resources/images` usando el mismo nombre de la página o publicación sin incluir la extensión. El nombre de cada fichero será un número entero basándonos en el orden de aparición de los mismos seguido de la extensión.

#### Tamaño
El tamaño de cada fichero no debería sobrepasar los 4 mebibytes pasegurar una rápida carga del sitio web y un menor impacto en red.

En caso de sobrepasar el tamaño, deberá ser considerado un recursos de tipo «Otros».

#### Resolución
La resolución de cada fichero no debería sobrepasar los 1280 píxeles de ancho para asegurar una rápida carga del sitio web y una buena representación.

En caso de no poderse disminuir y sobrepasar el tamaño, deberá ser considerado un recursos de tipo «Otros».

#### Formato
El formato escogido para las imágenes es [WebP](https://developers.google.com/speed/webp), en su versión de compresión con pérdida, para asegurar una rápida carga del sitio web y un menor impacto en la red.

Dicho formato ya está [ampliamente soportado](https://caniuse.com/webp), ofrece menor tamaño aplicando la misma tasa de compresión que JPEG y es bastante cercano a PNG en la visualización.

#### Conversión
Dado que muchas imágenes pre-existentes no se encuentran en el formato escogido, así como muchas utilidades aún no exportan al mismo por defecto, se incluyen instrucciones para poder realizar una conversión.

Las utilidades de WebP se pueden descargar desde los repositorios oficiales de algunas distribuciones como [webp](https://pkgs.org/download/webp) o [libwebp](https://pkgs.org/download/libwebp).

La utilidad `cwebp` es la utilizada para la conversión desde otros formatos. La conversión se realizaría de la siguiente forma:
```
cwebp -o 'fichero.webp' -q 75 -resize 1280 0 -- 'fichero.png'
```

En la línea de ejemplo sugerida arriba las opciones indicadas tienen distintos usos:
* `-o`: fichero de salida.
* `-q`: factor de compresión con pérdida entre 0 y 100.
* `-resize`: resolución en ancho por alto, si uno es 0 se aplica el valor que no lo es manteniendo la relación radio|aspecto.
* `--`: se escribe con un espacio luego y se especifica el fichero de entrada.

## Problemas
Aquí listan los problemas pendientes en la estructura del sitio web junto a sus posibles soluciones.

### Errores de formato HTML
Todas las páginas de +FLOSS tienen un formato de salida HTML sucio por defecto.

Este problema se debe a la redacción de las plantillas provistas por el tema que usamos en el sitio web aunque, no específicas del tema completamente sino que, aplica también a como se integran las que escribimos sin tomar en cuenta el nivel de identación y demás.

La solución propuesta, de manera temporal, es usar la extensión de Jekyll [Jekyll::Tidy](https://github.com/apsislabs/jekyll-tidy) que produce un buen formato de salida, aunque con ciertos errores mantenidos.

Se ha realizado un informe en el repositorio en GitHub donde se desarrolla el tema. Lo puedes visitar en [no-style-please#27](https://github.com/riggraz/no-style-please/issues/27).

### Nombres en la lista de publicaciones
La lista de publicaciones en la página principal, los nombres de las publicaciones aparecen en minúscula.

El problema existe porque la persona que desarrolló el tema que usamos en el sitio web, indicó en la plantilla que los títulos cambiasen su capitalización a minúsculas al ser listados.

La solución propuesta, de forma temporal, ha sido re-escribir dicha plantilla sin dicha transformación y situarla en el directorio `_includes/` con el mismo nombre de la plantilla original, `post_list.html`.

Se ha realizado un informe en el repositorio en GitHub donde se desarrolla el tema. Lo puedes visitar en [no-style-please#11](https://github.com/riggraz/no-style-please/issues/11).

### Sistema de comentarios
Jekyll, por defecto, no soporta sistema de comentarios nativo, y el tema actual no provee funcionalidad para ello.

En este sitio web se utiliza una instancia de [Isso](https://posativ.org/isso/) personalizada. 

Para integrar el sistema de comentarios, se ha re-escrito la pantilla base de las publicaciones, situada en el directorio `_layouts/` con el mismo nombre que la plantilla base original, `post.html`.

Hasta el momento, no se ha realizado informe en el repositorio de GitHub donde se desarrolla el tema.
