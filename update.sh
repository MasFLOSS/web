#!/bin/sh

# Salir inmediatamente si falla la ejecución de un comando
set -e
# Tratar variables indefinidas como errores en sustitución
set -u
# Mostrar comados ejecutados con sus argumentos
set -x

# Comprobar que existe directorio de la web
directorio="$HOME/web"
if [ "$PWD" != "$directorio" ]
then
    cd "$directorio"
fi

# Descargar cambios del repositorio principal
git pull --no-rebase --no-edit

# Actualizar dependencias de Ruby
bundle && bundle update

# Construir el sitio web
JEKYLL_ENV=production bundle exec jekyll build --trace

# Subir cambios al repositorio principal
git add Gemfile
git add Gemfile.lock
git commit -m "Actualizar Gemfile y Gemfile.lock"
git push origin main
