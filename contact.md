---
layout: page
lang: es
title: Contacto
---

## Canales
Actualmente, no disponemos de un canal general disponible en alguna platforma.

## Directo
Una forma directa de ponerse en contacto con +FLOSS es enviar un mensaje por correo electrónico a la dirección `masfloss [at] riseup [dot] net`.

Dado que esta dirección es de uso principal, puede ser accedida por el equipo de redacción y, por ello, recomendamos que cualquier persona se abstenga de enviar información personal y/o sensible.

No hace falta mencionar que el SPAM y otros tipos de «correo basura» quedan fuera de lo permitido para enviar a esta dirección.
